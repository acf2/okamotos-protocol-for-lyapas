*** ================================== FAQ ================================= ***
*** Q: Why is there (.../..., r) everywhere?                                 ***
*** A: (R)eturn code.                                                        ***
***                                                                          ***
*** Q: What does it mean?                                                    ***
*** A: 0 is "OK", 1 is "Not enough memory", 2 is "Division by 0".            ***
***    Didn't define others.                                                 ***
***                                                                          ***
*** Q: Why is there (L.., s, .../...) or (L.., v, .../...) everywhere?       ***
*** A: Capa(s)ity of a complex.                                              ***
***                                                                          ***
*** Q: For what purpose?                                                     ***
*** A: Some algorithms needed it. And standard                               ***
***    S<n> don't seem to work properly.                                     ***
***                                                                          ***
*** Q: I saw a function with (L1, .../L1, ...). Can't you just use           ***
***    (.../L1, ...) with no input complex?                                  ***
*** A: Again: I don't know why, but that form didn't work. That's all.       ***
***                                                                          ***
*** Q: Why is there some ..._p functions?                                    ***
*** A: It's predicates. They usually have 'a' in                             ***
***    out parameters. 'a' stands for (A)nswer:                              ***
***    true (1) or false (0).                                                ***
*** ======================================================================== ***


main(/)
		T⇒X

		127⇒t+1⇒x 256⇒z
		@+L1(4)	@+L2(8)
		@+L3(8)	@+L4(8)
		*Okamoto_generate_parameters(L1,4,L2,8,L3,8,L4,8,t,b/L1,L2,r) r↦34
		/'q = '>C *print_bn(L1/)
		/'p = '>C *print_bn(L2/)
		/'alpha_1 = '>C *print_bn(L3/)
		/'alpha_2 = '>C *print_bn(L4/)

		@+L5(4) @+L6(4) @+L7(8)
		*Okamoto_generate_key(L5,8,L6,8,L7,8,L1,L2,L3,L4/L5,L6,L7,r) r↦34
		/'Private key: ('>C *out_bn(L5/) /',\n              '>C *out_bn(L6/) /')\n'>C
		/'Public key: '>C *print_bn(L7/)

		/'\n-----\n\n'>C

		 @+L10(8) @+L8(4) @+L9(4)
		*generate_random_group_element(L8,4,L1/L8,r)
		*generate_random_group_element(L9,4,L1/L9,r)
		*Okamoto_commitment(L10,8,L2,L3,L4,L8,L9/L10,r) r↦34
		/'k_1 = '>C *print_bn(L8/)
		/'k_2 = '>C *print_bn(L9/)
		/'Witness (gamma) = '>C *print_bn(L10/)

		@+L11(4) *Okamoto_challenge(L11,4,t/L11,r) r↦34
		/'Request = '>C *print_bn(L11/)

		@+L12(4) @+L13(4)
		*Okamoto_response(L12,4,L13,4,L1,L5,L6,L8,L9,L11/L12,L13,r) r↦34
		/'y_1 = '>C *print_bn(L12/)
		/'y_2 = '>C *print_bn(L13/)
		
		*Okamoto_check(L2,L3,L4,L12,L13,L7,L11,L10/i)
		
		↑(i=1)1 /'FAILED\n'>C →2
	§1	/'PASSED\n'>C
	§2

	§34	/'Code: '>C *print(r,10/)
**


*** ======================== ***
*** === STANDARD LIBRARY === ***
*** ======================== ***


out(a,h/)
	a↦0 /'0'>C →4
	§0	@+F1(32)

		Oc
	§1	OZ a:h⇒a Z⇒x ↑(x>9)11 x+'0'⇒F1c →12
	§11	x-10+'A'⇒F1c
	§12	∆c a↦1
		c⇒Q1

		Q1⇒b Q1/2⇒e Oc
	§2	↑(c=e)3 b-c-1⇒d ⇔(F1cd) ∆c →2

	§3	/F1>C
		@-F1
	§4
**


print(a,h/)
	*out(a,h/) /'\n'>C
**


*** ================= ***
*** === AUXILIARY === ***
*** ================= ***


count_tail_zeros(n/r)
		Or
	§1	n<16 ↦2 n>16⇒n r+16⇒r
	§2	n<24 ↦3 n>8⇒n r+8⇒r
	§3	n<28 ↦4 n>4⇒n r+4⇒r
	§4	n<30 ↦5 n>2⇒n r+2⇒r
	§5	n<31 ↦6 n>1⇒n r+1⇒r
	§6
**


bitsize(L1/r)
		Q1-1⇒l l*32⇒a
		L1l⇒n 32⇒r
	§1	n>16 ↦2 n<16⇒n r-16⇒r
	§2	n>24 ↦3 n<8⇒n r-8⇒r
	§3	n>28 ↦4 n<4⇒n r-4⇒r
	§4	n>30 ↦5 n<2⇒n r-2⇒r
	§5	n>31 ↦6 n<1⇒n r-1⇒r
	§6	r+a⇒r
**


select_bit(n,L1/a)
		Oa
		n/32⇒h n;32⇒l L1h&Il↪2
	§1	1⇒a
	§2
**


set_bit(n,b,L1/L1)
		Oa
		n/32⇒h n;32⇒l
		b↪1 L1h∨Il⇒L1h →2
	§1	Il¬&L1h⇒L1h
	§2
**


*** ======================================= ***
*** === HUMAN INTERFACE FOR BIG NUMBERS === ***
*** ======================================= ***


*** --- Functions for output --- ***


out_bn(L1/)
		Q1⇒c↪2
	§1	∇c *out(L1c,16/) ↑(c=0)2 /' '>C →1
	§2
**


print_bn(L1/)
	*out_bn(L1/) /'\n'>C
**


*** --- Predicates --- ***


*** Does L1 equal n?
*** r == 1, if it does
*** r == 0, if it does not
equal_bn2sn_p(L1,n/a)
		Oa
		↑(Q1≠1)1 ↑(L1.0≠n)1
		1⇒a
	§1	
**


*** Does L1 equal L2?
*** a == 1, if it does
*** a == 0, if it does not
equal_bn2bn_p(L1,L2/a)
		↑(Q1>Q2)3 ↑(Q1<Q2)3

		Q1⇒i
	§1	↑(i=0)2 ∇i ↑(L1i≠L2i)3 →1

	§2	1⇒a →4
	§3	0⇒a
	§4
**


*** '>' for big numbers
*** a == 1, if L1 > L2
*** a == 0, otherwise
greater_than_p(L1,L2/a)
		↑(Q1>Q2)2 ↑(Q1<Q2)3

		Q1⇒i
	§1	↑(i=0)2 ∇i ↑(L1i<L2i)3 ↑(L1i>L2i)2 →1

	§2	1⇒a →4
	§3	0⇒a
	§4
**


*** --- Operations --- ***


*** Warning: very inelegent code
*** Reasons:
***	-1.	I have no idea why, accidently, S<n> returns wrong capacity
***		But it is the reason for (L1,s,.../L1,...) signatures
***
*** 0.	All operations may use memory beyond size (Q) of a complex
***		but they do not clean up.
***		Hence the reduce and zero functions.
***
*** 1.	L2⇒L1 DOES NOT WORK!
*** 	EVEN IF CAPACITIES (S) AND SIZES (Q) ARE EQUAL!
***		[INTERNAL ERROR] AT RUNTIME
*** 	The only way to make it work
*** 	is to give destination complex abnormal size (>128).
*** 	I have no idea why, and I've had enough of that.
*** 	Hence the assign/assign_base functions and 
***		all that temporary complexes with capacity of 512.
***
*** 2.
***		1)	In one case multiplication
***			required (?) up to Q1*Q2 memory. I'm not so sure
***			about "required" part, but capacity of Q1+Q2 was totally not enough.
***			This is awful. Capped on 512.
***
***		2)	Also, when multiplying L1*L2⇒L3 there must be at list Q1+Q2 memory IN PLACE.
***			Id est in L1. Why? Maybe my program didn't used it in a correct manner?
***
*** 3.	Reduce is not required after all custom complex operations.
***		But for all integrated operations (+,-,*,/, ...) or raw work with complexes it's crucial.
***		Exempli gratia: generate_superprime will stuck without explicit assign_base of L3.
***		So, prior to use, complex MUST be reduce'd, zero'ed, assign'ed or assign_base'd.


*** Reduce length of big number to correct value
*** And set everything, that is not used, to zero
*** (I personally catched several BAD bugs without this)
reduce(L1,s/L1)
		Q1⇒i↪2 ∇i
	§1	↑(L1i≠0)2 i↪2 ∇i →1
	§2	i+1⇒Q1
	§3	Q1⇒i
	§4	↑(i=s)5 OL1i ∆i →4
	§5
**


zero(L1,s/L1)
		Oi
	§0	↑(i=s)1 OL1i ∆i →0
	§1	1⇒Q1
**

*** Equivalent to
***     mov L1, n
assign_base(L1,s,n/L1,r)
		↑(s<1)2
		0⇒r *** 'success'

		1⇒i 1⇒Q1 n⇒L1.0
	§1	↑(i=s)3 OL1i ∆i →1
	§2	1⇒r *** 'failure'
	§3
**


*** Equivalent to
***     mov L1, L2
assign(L1,s,L2/L1,r)
		Q2⇒q
		↑(q>s)2
		0⇒r *** 'success'

		Oi q⇒Q1
	§0	↑(i=q)1 L2i⇒L1i ∆i →0
	§1	↑(i=s)3    OL1i ∆i →1
	§2	1⇒r *** 'failure'
	§3
**


*** Equivalent to
***     add L1, L2
add(L1,s,L2/L1,r)
		↑(Q1<Q2)1 Q1+1⇒q →2
	§1	Q2+1⇒q
	§2	
		0⇒r *** 'success'
		@+L3(512) *assign(L3,512,L1/L3,r)

		q⇒Q3 L3+L2⇒L3 *reduce(L3,512/L3) ↑(Q3>s)4
	§3	*assign(L1,s,L3/L1,r) →5
	§4	1⇒r *** 'failure'
	§5
**


*** Equivalent to
***     sub L1, L2
*** That error with subtraction IS present
*** hence L4 complex
sub(L1,s,L2/L1,r)
		0⇒r *** 'success'
		*greater_than_p(L2,L1/a) a↪2
	§1	1⇒Q1 OL1.0 →3
	§2	Q1⇒q @+L3(512) *zero(L3,512/L3)
		     @+L4(q) *assign(L4,q,L2/L4,r)

		q⇒Q4 L1-L4⇒L3
	§3	*reduce(L3,512/L3) ↑(Q3>s)5
	§4	*assign(L1,s,L3/L1,r) →6
	§5	1⇒r *** 'failure'
	§6
**


*** Equivalent to
***     shr L1, L2
shr(L1,s,n/L1,r)
		0⇒r *** 'success'
		*bitsize(L1/a) ↑(n<a)2
	§1	1⇒Q1 OL1.0 →3
	§2	Q1⇒q @+L3(512) *zero(L3,512/L3)

		L1>n⇒L3
	§3	*reduce(L3,512/L3)
	§4	*assign(L1,s,L3/L1,r)
**


*** No direct equivalent in assembly
*** Somewhat similar to
***     mov rax, L1
***     mul L2
mul(L1,s,L2/L1,r)
		*equal_bn2sn_p(L2,1/a) a↦3
		Q1+Q2⇒q
		
		0⇒r *** 'success'		
	§0	↑(q>512)2 @+L3(512) *assign(L3,512,L1/L3,r) r↦3
		*** XXX: I don't know why, but without q⇒Q1 (prior to multipliation) this just doesn't seem to work.
		***      That is why there are an assignment to L3 and "q⇒Q3" thing.
		q⇒Q3 L3*L2⇒L3 q⇒Q3 *reduce(L3,512/L3) ↑(Q3>s)2
	§1	*assign(L1,s,L3/L1,r) →3
	§2	1⇒r *** 'failure'
	§3
**


*** No direct equivalent in assembly
*** Somewhat similar to
***     mov rdx, 0
***     mov rax, L1
***     div L2
***     mov L1, rdx ; remainder
mod(L1,s,L2/L1,r)
		*greater_than_p(L2,L1/a) a↦3
		Q2⇒q
		q↪2 *equal_bn2sn_p(L2,0/a) a↦2
		0⇒r *** 'success'

		@+L3(512) *assign(L3,512,L2/L3,r) r↦3

		L1:L3 q⇒Q3 *reduce(L3,512/L3)
	§1	*assign(L1,s,L3/L1,r) →3
	§2	2⇒r *** 'division by zero'
	§3
**


*** No direct equivalent in assembly
*** Somewhat similar to
***     mov rdx, 0
***     mov rax, L1
***     div L2
***     mov L1, rax ; quotionent
div(L1,s,L2/L1,r)
		*greater_than_p(L2,L1/a) a↪0
		*zero(L1,s/L1) →3

	§0	Q1-Q2+1⇒q
		Q2↪2 *equal_bn2sn_p(L2,0/a) a↦2
		0⇒r *** 'success'

		@+L3(512) *assign(L3,512,L2/L3,r) r↦3

		L1/L3⇒L3 q⇒Q3 *reduce(L3,512/L3)
	§1	*assign(L1,s,L3/L1,r) →3
	§2	2⇒r *** 'division by zero'
	§3
**


*** Fill n bits of complex randomly
*** There is no guarantee that number will actually be n-bit long
*** For guaranteed n-bit random number use "generate_random"
random_fill(L1,s,n/L1,r)
		n/32⇒e n;32⇒c c↪0 e+1⇒e

	§0	↑(e>s)5
	§1	0⇒r *** 'success'

		Oi
	§2	↑(i=e)3 X⇒L1i ∆i →2
	§3	e⇒Q1-1⇒q c↪4
		Ic-1⇒m c-1⇒t X&m⇒L1q
	§4	*reduce(L1,s/L1) →6
	§5	1⇒r *** 'failure'
	§6	
**


*** Random n-bit big number
*** L1 should have enough place for it, obviously
generate_random(L1,s,n/L1,r)
		n/32⇒e n;32⇒c c↪0 e+1⇒e

	§0	↑(e>s)6
	§1	0⇒r *** 'success'

		Oi
	§2	↑(i=e)3 X⇒L1i ∆i →2
	§3	e⇒Q1-1⇒q c↪4
		Ic-1⇒m c-1⇒t X&m∨It⇒L1q →5
	§4	X∨I31⇒L1q
	§5	*reduce(L1,s/L1) →7
	§6	1⇒r *** 'failure'
	§7	
**


*** =============================== ***
*** === NUMBER THEORY FUNCTIONS === ***
*** =============================== ***


*** L1 * L2 (mod L3) ⇒ L1
*** L1 should have enough memory to store min(L1 * L2, L3)
mulmod(L1,s,L2,L3/L1,r)
		Or
		Q1+Q2⇒q Q3*2⇒t ↑(t≥q)0
		q⇒t
	§0	@+L4(t) *assign(L4,t,L1/L4,r) r↦1
		*mul(L4,t,L2/L4,r) r↦1
		*mod(L4,t,L3/L4,r) r↦1
	§1	*assign(L1,s,L4/L1,r)
**


extract_power_of_2(L1/r)
		Or Oi
	§1	L1i↦2 r+32⇒r ∆i →1
	§2	*count_tail_zeros(L1i/a) r+a⇒r
***


*** L1 ^ L2 (mod L3) ⇒ L1
*** L1 should have enough memory to store min(L1 ^ L2, L3)
powermod(L1,s,L2,L3/L1,r)
		Or Q1+Q2+1⇒q Q3+1⇒t ↑(t≥q)0 q⇒t
	§0	@+L4(t) *assign_base(L4,t,1/L4,r) r↦34 *** accumulator

		*bitsize(L2/b) ∇b *** bit

	§1	*select_bit(b,L2/a) a↪2 *mulmod(L4,t,L1,L3/L4,r) r↦34 *** /'	powermod.mulmod1: '>C *print_bn(L4/)
	§2	b↪3 *mulmod(L4,t,L4,L3/L4,r) r↦34 *** /'	powermod.mulmod2: '>C *print_bn(L4/)
		∇b →1
	§3	*assign(L1,s,L4/L1,r)
	§34
**


*** Miller-Rabin primality test
*** t - reliability parameter
*** a == 0, L1 - composite
*** a == 1, L1 - prime (may be returned for composite with probability 1/4^t)
is_prime_p(L1,s,t/a,r)
		Or t⇒z s⇒x

		*** L2 := L1 (L1 == n)
		@+L2(x) *assign(L2,x,L1/L2,r) r↦34

		*** L3 will later become 'n-3'
		*** (for generating numbers in [0, n-4], then shift range to [2, n-2])
		@+L3(x) *** NOTE: L3 will later be assigned

		*** L4 := 1
		@+L4(1) *assign_base(L4,1,1/L4,r) r↦34

		*** L5 := -1 (mod n)
		@+L5(x) *assign(L5,x,L1/L5,r) r↦34
		*sub(L5,x,L4/L5,r) r↦34

		*** n = 2^s * r + 1
		*** L2 := r
		*sub(L2,x,L4/L2,r) r↦34
		*extract_power_of_2(L2/s)
		*shr(L2,x,s/L2,r) r↦34
		
		*** L4 := 3
		*assign_base(L4,1,3/L4,r)

		*** L3 := n - 3 = L1 - 3
		*assign(L3,x,L1/L3,r) r↦34
		*sub(L3,x,L4/L3,r) r↦34

		*** L4 := 2
		*assign_base(L4,1,2/L4,r)

		*** L6 <=> b,y from Miller-Rabin algorithm
		x+1⇒h @+L6(h)

		1⇒a Oi Q1*32⇒b
		*** for i from 0 to t-1
	§1	↑(i=z)34

		*** Random 2 ≤ b ≤ n-2
		*generate_random(L6,h,b/L6,r) r↦34
		*mod(L6,h,L3/L6,r) r↦34
		*add(L6,h,L4/L6,r) r↦34

		*** y := b ^ r (mod n)
		*powermod(L6,h,L2,L1/L6,r) r↦34

		*** if y ≠ +-1
		*equal_bn2sn_p(L6,1/t) t↦4		
		*equal_bn2bn_p(L6,L5/t) t↦4

		1⇒j
		*** while (y ≠ -1 & j < s)
	§2	*equal_bn2bn_p(L6,L5/t) t↦3 ↑(j≥s)3

		*** y := y^2 mod n
		*mulmod(L6,h,L6,L1/L6,r) r↦34

		*** if y = 1 then L1 - composite
		*equal_bn2sn_p(L6,1/t) t↦33
		
		∆j →2

		*** if y ≠ -1 then L1 - composite
	§3	*equal_bn2bn_p(L6,L5/t) t↪33

	§4	∆i →1
		
	§33	Oa
	§34
**

*** L1 should have enough size for it
*** b - bitsize
*** t - reliability for Miller-Rabin test
generate_prime(L1,s,b,t/L1,r)
		Or
		b;32⇒c b/32⇒d c↪0 ∆d
	§0	↑(d>s)33

	§1	d⇒Q1 Ic-1⇒m

	§3	*generate_random(L1,s,b/L1,r) r↦34
		*set_bit(0,1,L1/L1) *is_prime_p(L1,s,t/a,r) r↦34 a↪3 →34
	§33	1⇒r
	§34  
**


*** For generating such p, that q|(p-1)
*** b - bitsize of superprime
*** t - reliability for Miller-Rabin test
generate_superprime(L1,s,b,L2,t/L1,r)
		Or
		b;32⇒c b/32⇒d c↪0 ∆d
	§0	*bitsize(L2/a) b-a⇒f
		f;32⇒g f/32⇒h g↪1 ∆h
	§1	↑(d>s)33

		@+L3(1) *assign_base(L3,1,1/L3,r) r↦34

	§2	*generate_random(L1,s,f/L1,r) r↦34
		*set_bit(0,0,L1/L1)
		*mul(L1,s,L2/L1,r) r↦34
		*add(L1,s,L3/L1,r) r↦34
		*bitsize(L1/a) ↑(a≠b)2 
		*is_prime_p(L1,s,t/a,r) r↦34 a↪2 →34
	§33	1⇒r
	§34
**

*** Order of L2 in (Z_L3*, * mod L3)
*** XXX: Slow, do not use with bitsize(L3) > ~30
order(L1,s,L2,L3/L1,r)
		Q2⇒s 
		*assign_base(L1,s,1/L1,r) r↦34
		@+L4(s) *assign(L4,s,L2/L4,r) r↦34
		@+L6(1) *assign_base(L6,1,1/L6,r) r↦34 *** const 1

	§1	*equal_bn2sn_p(L4,1/a) ↑(a=1)34
		*mulmod(L4,s,L2,L3/L4,r) r↦34
		*add(L1,s,L6/L1,r) r↦34
		→1
	§34
**


*** L1 from (Z_L2*, * mod L2)
*** L2 must be prime
generate_random_group_element(L1,s,L2/L1,r)
		Or
		@+L3(1) *assign_base(L3,1,2/L3,r) r↦34
		@+L4(s) *assign(L4,s,L2/L4,r) r↦34
		*sub(L4,s,L3/L4,r) r↦34

		*assign_base(L3,1,1/L3,r) r↦34
		*bitsize(L2/b) *generate_random(L1,s,b/L1,r) r↦34
		*mod(L1,s,L4/L1,r) r↦34
		*add(L1,s,L3/L1,r) r↦34
	§34
**


*** Here:
***   L2 - module for group (group order is p-1)
***   L3 - desired element order, q|(p-1)
generate_group_element_with_fixed_order(L1,s,L2,L3/L1,r)
		Or

		*** d = (p-1)/q
		Q2⇒v
		@+L4(v) *assign(L4,v,L2/L4,r) r↦34
		@+L5(1) *assign_base(L5,1,1/L5,r) r↦34
		*sub(L4,v,L5/L4,r) r↦34
		*div(L4,v,L3/L4,r) r↦34

		*** h := h ^ d mod p
		*** if h != 1 then h - required element
	§1	*generate_random_group_element(L1,s,L2/L1,r) r↦34
		*powermod(L1,s,L4,L2/L1,r) r↦34
		*equal_bn2sn_p(L1,1/a) ↑(a=1)1
	§34
**


*** ======================================= ***
*** === OKAMOTO IDENTIFICATION PROTOCOL === ***
*** ======================================= ***


*** From protocol
***   t - reliability
***       t+1 = c - desired bitsize of q (c*2 - lenght of key)
***   b - desired bitsize of p,
***       but if b<c then b := 2*c
*** out:
***   L1 - q
***   L2 - p
***   L3 - alpha_1
***   L4 - alpha_2
Okamoto_generate_parameters(L1,s,L2,v,L3,w,L4,x,t,b/L1,L2,r)
		Or t+1⇒c*2⇒d
		↑(c<b)1 d⇒b
	§1	*generate_prime(L1,s,c,40/L1,r) r↦34
		*generate_superprime(L2,v,b,L1,40/L2,r) r↦34
		*generate_group_element_with_fixed_order(L3,w,L2,L1/L3,r) r↦34
	§2	*generate_group_element_with_fixed_order(L4,w,L2,L1/L4,r) *equal_bn2bn_p(L4,L3/a) ↑(a=1)2
	§34
**


*** From protocol:
***   L4 - q
***   L5 - p
***   L6, L7 - alpha_1, alpha_2
*** out:
***   L1, L2 - a_1, a_2 (private key)
***   L3 - v (public key)
***
*** Now THAT function signature is terrifying
Okamoto_generate_key(L1,s,L2,v,L3,w,L4,L5,L6,L7/L1,L2,L3,r)
		Or
		*** L1 := a_1
	§1	*generate_random_group_element(L1,s,L4/L1,r) r↦34
		*equal_bn2sn_p(L1,1/a) ↑(a=1)1

		*** L2 := a_2
	§2	*generate_random_group_element(L2,v,L4/L2,r) r↦34
		*equal_bn2sn_p(L2,1/a) ↑(a=1)2
		*equal_bn2bn_p(L2,L1/a) ↑(a=1)2

		*** v = alpha_1^(-a_1) * alpha_2^(-a_2)
		@+L8(1) *assign_base(L8,1,1/L8,r) r↦34
		Q5⇒q

		*** L9 := p-1-a_1 = -a_1 mod (p-1)
		*** L11 := p-1-a_2 = -a_2 mod (p-1)
		@+L9(q) *assign(L9,q,L5/L9,r) r↦34
		*sub(L9,q,L8/L9,r) r↦34
		@+L11(q) *assign(L11,q,L9/L11,r) r↦34
		*sub(L9,q,L1/L9,r) r↦34
		*sub(L11,q,L2/L11,r) r↦34

		*** L10 := alpha_1^(-a_1)
		@+L10(q) *assign(L10,q,L6/L10,r) r↦34
		*powermod(L10,q,L9,L5/L10,r) r↦34

		*** L12 := alpha_2^(-a_2)
		@+L12(q) *assign(L12,q,L7/L12,r) r↦34
		*powermod(L12,q,L11,L5/L12,r) r↦34

		*** L3 := v = alpha_1^(-a_1) * alpha_2^(-a_2)
		*mulmod(L10,q,L12,L5/L10,r) r↦34
		*assign(L3,w,L10/L3,r) r↦34
	§34
**


*** From protocol
***   L2 - p
***   L3, L4 - alpha_1, alpha_2
***   L5, L6 - k_1, k_2
*** out:
***   L1 - gamma (witness)
Okamoto_commitment(L1,s,L2,L3,L4,L5,L6/L1,r)
		Or Q2⇒q
		@+L7(q) *assign(L7,q,L3/L7,r) r↦34
		*powermod(L7,q,L5,L2/L7,r) r↦34

		@+L8(q) *assign(L8,q,L4/L8,r) r↦34
		*powermod(L8,q,L6,L2/L8,r) r↦34

		*mulmod(L7,q,L8,L2/L7,r) r↦34
		*assign(L1,s,L7/L1,r)
	§34
**


*** From Okamoto's protocol
***   t - reliability parameter
Okamoto_challenge(L1,s,t/L1,r)
		Or
		t;32⇒c t/32⇒d c↪1 ∆d
	§1	↑(d>s)33
		@+L2(d) *zero(L2,s/L2) *set_bit(t,1,L2/L2)
		t-1⇒b *random_fill(L1,s,b/L1,r) r↦34
		@+L3(1) *assign_base(L3,1,1/L3,r) r↦34
		*add(L1,s,L3/L1,r) →34
	§33 1⇒r
	§34
**


*** From protocol
***   L3 - q
***   L4, L5 - a_1, a_2 (private key)
***   L6, L7 - k_1, k_2
***   L8 - r (request)
*** out:
***   L1, L2 - y_1, y_2 (response)
Okamoto_response(L1,s,L2,v,L3,L4,L5,L6,L7,L8/L1,L2,r)
		Or
		Q3+1⇒q @+L9(q)
		*** y_1
		*assign(L9,q,L4/L9,r) r↦34
		*mulmod(L9,q,L8,L3/L9,r) r↦34
		*add(L9,q,L6/L9,r) r↦34
		*mod(L9,q,L3/L9,r) r↦34
		*assign(L1,s,L9/L1,r) r↦34

		*** y_2
		*assign(L9,q,L5/L9,r) r↦34
		*mulmod(L9,q,L8,L3/L9,r) r↦34
		*add(L9,q,L7/L9,r) r↦34
		*mod(L9,q,L3/L9,r) r↦34
		*assign(L2,v,L9/L2,r)
	§34
**


*** From protocol
***   L1 - p
***   L2,L3 - alpha_1, alpha_2
***   L4,L5 - y_1, y_2 (response)
***   L6 - v (public key)
***   L7 - r (request)
***   L8 - gamma (witness)
***   i - identified? (1 - true, 0 - false)
Okamoto_check(L1,L2,L3,L4,L5,L6,L7,L8/i)
		Or Q1⇒q

		@+L9(q) @+L10(q) @+L11(q)

		*** alpha_1 ^ y_1
		*assign(L9,q,L2/L9,r) r↦34
		*powermod(L9,q,L4,L1/L9,r) r↦34

		*** alpha_2 ^ y_2
		*assign(L10,q,L3/L10,r) r↦34
		*powermod(L10,q,L5,L1/L10,r) r↦34

		*** v ^ r
		*assign(L11,q,L6/L11,r) r↦34
		*powermod(L11,q,L7,L1/L11,r) r↦34

		*** L9 := alpha_1^y_1 * alpha_2^y_2 * v^r
		*mulmod(L9,q,L10,L1/L9,r) r↦34
		*mulmod(L9,q,L11,L1/L9,r) r↦34

		*** L9 =? L8 = gamma
		*equal_bn2bn_p(L9,L8/i)
	§34
**
