LTCDIR=../ltc
EXEC=okamoto
SRC=okamoto.l
ASMSRC=$(patsubst %.l,%.s,$(SRC))

$(EXEC): $(SRC) allbn.o
	$(LTCDIR)/ltc $(EXEC).l

edit:
	gedit $(SRC) >/dev/null 2>&1 &	

allbn.o:
	ln -s $(LTCDIR)/allbn.o allbn.o

clean:
	-rm -f $(ASMSRC) $(EXEC) allbn.o

.PHONY: clean